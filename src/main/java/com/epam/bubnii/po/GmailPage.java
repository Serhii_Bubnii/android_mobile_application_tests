package com.epam.bubnii.po;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebDriver;

public class GmailPage extends PageObject {

    @AndroidFindBy(id = "com.google.android.gm:id/action_done")
    private MobileElement takeMeToGmailButton;
    @AndroidFindBy(id = "com.google.android.gm:id/compose_button")
    private MobileElement composeButton;
    @AndroidFindBy(id = "com.google.android.gm:id/to")
    private MobileElement receiverField;
    @AndroidFindBy(id = "com.google.android.gm:id/subject")
    private MobileElement subjectField;
    @AndroidFindBy(id = "com.google.android.gm:id/body")
    private MobileElement messageField;
    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc='Send']")
    private MobileElement sendButton;
    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc='Open navigation drawer']")
    private MobileElement menuButton;
    @AndroidFindBy(xpath = "//android.widget.ListView/android.widget.LinearLayout[8]")
    private MobileElement sentMessagesButton;
    @AndroidFindBy(xpath = "//android.widget.ListView/android.widget.FrameLayout[1]/android.view.View")
    private MobileElement lastSentMessage;
    @AndroidFindBy(id = "com.google.android.gm:id/subject_and_folder_view")
    private MobileElement conversationHeader;
    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"Delete\"]")
    private MobileElement deleteMessageButton;
    @AndroidFindBy(id = "com.google.android.gm:id/action_text")
    private MobileElement undoDeletingButton;

    public GmailPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void tapTakeMeToGmailButton(){
        takeMeToGmailButton.click();
    }

    public void tapComposeButton() {
        composeButton.click();
    }

    public void fillReceiverField(String receiver) {
        receiverField.sendKeys(receiver);
    }

    public void fillSubjectField(String subject) {
        subjectField.sendKeys(subject);
    }

    public void fillMessageField(String message) {
        messageField.sendKeys(message);
    }

    public void tapSendButton() {
        sendButton.click();
    }

    public void tapMenuButton() {
        menuButton.click();
    }

    public void tapSentMessagesButton() {
        sentMessagesButton.click();
    }

    public void tapOnLastSentMessage() {
        lastSentMessage.click();
    }

    public String getSubjectFormLastSentMessage(){
        return conversationHeader.getText();
    }

    public void tapDeleteMessage() {
        deleteMessageButton.click();
    }

    public MobileElement getUndoDeletingButton() {
        return undoDeletingButton;
    }
}
