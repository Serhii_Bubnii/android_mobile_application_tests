package com.epam.bubnii.po;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebDriver;

public class AuthorizationPage extends PageObject {
    @AndroidFindBy(id = "com.google.android.gm:id/welcome_tour_got_it")
    private MobileElement gotItButton;
    @AndroidFindBy(id = "com.google.android.gm:id/setup_addresses_add_another")
    private MobileElement addAnotherEmailButton;
    @AndroidFindBy(id = "com.google.android.gm:id/google_option")
    private MobileElement googleOption;
    @AndroidFindBy(id = "com.google.android.gm:id/suw_navbar_next")
    private MobileElement nextButton;
    @AndroidFindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[3]/" +
            "android.view.View/android.view.View[1]/android.view.View[1]/android.widget.EditText")
    private MobileElement emailInput;
    @AndroidFindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[4]/android.widget.Button")
    private MobileElement submitEmailButton;
    @AndroidFindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[3]/android.view.View/" +
            "android.view.View/android.view.View/android.view.View/android.view.View[1]/android.widget.EditText")
    private MobileElement passwordInput;
    @AndroidFindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[4]/android.widget.Button")
    private MobileElement submitPasswordButton;
    @AndroidFindBy(xpath = "//android.webkit.WebView/android.webkit.WebView/android.view.View[1]" +
            "/android.view.View[6]/android.widget.Button")
    private MobileElement skipButton;
    @AndroidFindBy(xpath = "//android.webkit.WebView/android.view.View[1]/android.view.View[4]/android.widget.Button")
    private MobileElement agreeWithTermsButton;

    public AuthorizationPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void tapOnGotItButton(){
        gotItButton.click();
    }

    public void addAnotherEmail(){
        addAnotherEmailButton.click();
    }

    public void setGoogleOption(){
        googleOption.click();
    }

    public void submitOption(){
        nextButton.click();
    }

    public void fillEmail(String email) {
        emailInput.sendKeys(email);
    }

    public void submitEmail() {
        submitEmailButton.click();
    }

    public void fillPassword(String password) {
        passwordInput.sendKeys(password);
    }

    public void submitPassword() {
        submitPasswordButton.click();
    }

    public void pressSkipButton(){
        skipButton.click();
    }

    public void pressAgreeWithTermsButton(){
        agreeWithTermsButton.click();
    }

}
