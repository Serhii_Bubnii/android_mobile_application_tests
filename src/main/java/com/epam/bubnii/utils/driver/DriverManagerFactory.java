package com.epam.bubnii.utils.driver;

import com.epam.bubnii.utils.helper.PropertyReader;

public class DriverManagerFactory {

    public static AbstractDriverManager getManager() {
        AbstractDriverManager driverManager;
        DriverType type = DriverType.valueOf(PropertyReader.getProperties("driver").toUpperCase());
        switch (type) {
            case ANDROID:
                driverManager = new AndroidDriverManager();
                break;
            case IOS:
                driverManager = new IphoneDriverManager();
                break;
            default:
                driverManager = new AndroidDriverManager();
                break;
        }
        return driverManager;
    }

}
