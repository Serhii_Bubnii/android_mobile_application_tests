package com.epam.bubnii.utils.driver;

import io.appium.java_client.android.AndroidDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.TimeUnit;

public class AndroidDriverManager extends AbstractDriverManager {

    private static Logger LOGGER = LogManager.getLogger(AndroidDriverManager.class);

    @Override
    protected void createDriver() {
        LOGGER.info("Create Android driver");
        driver = new AndroidDriver<>(url, setCapabilities());
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

}
