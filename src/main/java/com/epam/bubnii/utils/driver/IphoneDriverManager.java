package com.epam.bubnii.utils.driver;

import io.appium.java_client.ios.IOSDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.TimeUnit;

public class IphoneDriverManager extends AbstractDriverManager{

    private static Logger LOGGER = LogManager.getLogger(IphoneDriverManager.class);

    @Override
    protected void createDriver() {
        LOGGER.info("Create Iphone driver");
        driver = new IOSDriver<>(url, setCapabilities());
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }
}
