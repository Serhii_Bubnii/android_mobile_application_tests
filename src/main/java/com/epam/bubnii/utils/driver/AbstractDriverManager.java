package com.epam.bubnii.utils.driver;

import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

import static com.epam.bubnii.utils.helper.PropertyReader.getProperties;

public abstract class AbstractDriverManager {

    private static Logger LOGGER = LogManager.getLogger(AbstractDriverManager.class);
    protected WebDriver driver;
    protected static URL url;

    static {
        try {
            url = new URL("http://127.0.0.1:4723/wd/hub");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    protected abstract void createDriver();

    public WebDriver getDeviceDriver() {
        LOGGER.info("Get device driver");
        if (null == driver) {
            LOGGER.info("Create device driver");
            createDriver();
        }
        return driver;
    }

    protected static DesiredCapabilities setCapabilities() {
        LOGGER.info("Set driver capabilities");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, getProperties("PLATFORM_NAME"));
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, getProperties("DEVICE_NAME"));
        capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, getProperties("APP_PACKAGE"));
        capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, getProperties("APP_ACTIVITY"));
        return capabilities;
    }

    public void quitDeviceDriver() {
        LOGGER.info("Quit driver");
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }

}
