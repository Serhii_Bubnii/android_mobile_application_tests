package com.epam.bubnii.utils.helper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyReader {

    private static Logger LOGGER = LogManager.getLogger(PropertyReader.class);

    public static String getProperties(String property) {
        LOGGER.info("get property file");
        Properties properties = new Properties();
        InputStream inputStream = null;
        try {
            LOGGER.info("Read property file");
            inputStream = new FileInputStream(System.getProperty("user.dir" + "/src/main/resources/my.properties"));
            LOGGER.info("Load property file");
            properties.load(inputStream);
        } catch (IOException e) {
            LOGGER.info("IOException " + e.getStackTrace());
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return properties.getProperty(property);
    }

}
