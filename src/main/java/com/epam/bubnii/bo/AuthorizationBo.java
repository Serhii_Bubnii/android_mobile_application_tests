package com.epam.bubnii.bo;

import com.epam.bubnii.model.User;
import com.epam.bubnii.po.AuthorizationPage;
import org.openqa.selenium.WebDriver;

public class AuthorizationBo {

    private AuthorizationPage authorizationPage;

    public AuthorizationBo(WebDriver webDriver) {
        authorizationPage = new AuthorizationPage(webDriver);
    }

    public void login(User user){
        authorizationPage.tapOnGotItButton();
        authorizationPage.addAnotherEmail();
        authorizationPage.setGoogleOption();
        authorizationPage.submitOption();
        authorizationPage.fillEmail(user.getEmail());
        authorizationPage.submitEmail();
        authorizationPage.fillPassword(user.getPassword());
        authorizationPage.submitPassword();
        authorizationPage.pressAgreeWithTermsButton();
    }

}
