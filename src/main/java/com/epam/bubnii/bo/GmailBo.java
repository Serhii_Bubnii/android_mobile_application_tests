package com.epam.bubnii.bo;

import com.epam.bubnii.model.Email;
import com.epam.bubnii.po.GmailPage;
import org.openqa.selenium.WebDriver;

public class GmailBo {

    private GmailPage gmailPage;

    public GmailBo(WebDriver webDriver){
        gmailPage = new GmailPage(webDriver);
    }

    public void sendMessage(Email email) {
        gmailPage.tapComposeButton();
        gmailPage.fillReceiverField(email.getReceiver());
        gmailPage.fillSubjectField(email.getSubject());
        gmailPage.fillMessageField(email.getMessage());
        gmailPage.tapSendButton();
    }

    public boolean deleteJustWrittenMessage(Email email) {
        gmailPage.tapMenuButton();
        gmailPage.tapSentMessagesButton();
        gmailPage.tapOnLastSentMessage();
        if(gmailPage.getSubjectFormLastSentMessage().toLowerCase()
                .contains(email.getSubject().toLowerCase())){
            gmailPage.tapDeleteMessage();
            return true;
        }
        return false;
    }

    public boolean isUndoDeletingButtonDisplayed() {
        return gmailPage.getUndoDeletingButton().isDisplayed();
    }
}
