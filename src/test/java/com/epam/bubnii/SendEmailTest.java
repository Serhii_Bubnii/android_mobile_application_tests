package com.epam.bubnii;

import com.epam.bubnii.bo.AuthorizationBo;
import com.epam.bubnii.bo.GmailBo;
import com.epam.bubnii.model.Email;
import com.epam.bubnii.model.User;
import com.epam.bubnii.utils.driver.AbstractDriverManager;
import com.epam.bubnii.utils.driver.DriverManagerFactory;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.epam.bubnii.utils.helper.PropertyReader.getProperties;
import static org.testng.Assert.assertTrue;

public class SendEmailTest {

    protected static WebDriver webDriver;
    protected static AbstractDriverManager driverManager;
    private AuthorizationBo authorizationBo;
    private GmailBo gmailBo;

    @BeforeClass(alwaysRun = true)
    public static void setUp() {
        driverManager = DriverManagerFactory.getManager();
        webDriver = driverManager.getDeviceDriver();
    }

    @Test
    public void testSendEmail(String userEmail, String userPassword) {
        authorizationBo = new AuthorizationBo(webDriver);
        gmailBo = new GmailBo(webDriver);
        authorizationBo.login(
                new User()
                        .setEmail(getProperties("EMAIL_LOGIN"))
                        .setPassword(getProperties("EMAIL_PASSWORD")));
        gmailBo.sendMessage(
                new Email()
                        .setReceiver(getProperties("USER_EMAIL_RECEIVER"))
                        .setReceiverCopy(getProperties("USER_EMAIL_CC"))
                        .setReceiverHiddenCopy("USER_EMAIL_BCC")
                        .setSubject("EMAIL_SUBJECT")
                        .setMessage("EMAIL_MESSAGE"));
        assertTrue(gmailBo.isUndoDeletingButtonDisplayed());
    }

    @AfterClass
    public static void tearDown() {
        driverManager.quitDeviceDriver();
    }

}
